import { useEffect, useState } from "react";
import { BsFillEyeFill, BsFillEyeSlashFill, BsShieldFillCheck } from "react-icons/bs";
import { FaLock } from "react-icons/fa";
import { useNavigate, useParams } from "react-router-dom";
import API from "../../Api";
import Loading from "../../components/Common/Loading/Loading";
import { useToasts } from "../../hooks/Toasts/ToastContext";

interface IProps {
    password: string;
    confirmPassword: string;
  }
  
  const initialValue = {
    password: '',
    confirmPassword: ''
  }

const ResetPassword = () => {

    const navigate = useNavigate();
    const [validUrl, setValidUrl] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(true)
    const { id, token } = useParams();
    const url = `${process.env.REACT_APP_API}/auth/${id}/${token}`;

    const { pushToast } = useToasts();

    const [formDatas, setFormDatas] = useState<IProps>(initialValue);
    const [isShown, setIsShown] = useState<boolean>(false);
    const [isConfirm, setIsConfirm] = useState<boolean>(false);
    const [isError, setIsError] = useState<boolean>(false);
    const [errorMessage, setErrorMessage] = useState<string>('');

    useEffect(() => {
        const verifyEmailUrl = async () => {
            try {
                const { data } = await API.get(url);
                setLoading(false)
                console.log(data)
                setValidUrl(true)
            } catch (err) {
                console.log(err);
                setLoading(false)
                setValidUrl(false);
            }
        }

        verifyEmailUrl();
    }, [id, token]);

    const handleChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
        e.preventDefault();
        setFormDatas({
            ...formDatas,
            [e.target.name]: e.target.value
        })
        if(isError ) {
            setIsError(false);
            setErrorMessage('');
        }
    }

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        if(formDatas.password === formDatas.confirmPassword){
            try{
                await API.post(url,formDatas);
                pushToast({
                    title: "Mot de passe",
                    content: "Votre nouveau mot de passe a bien été enregistré"
                })
                navigate('/');
            }    
            catch(err: any){
                const errors = err.data.errors;
                errors.forEach((e: any) => {
                    if(e.param === 'email' ){
                        setErrorMessage(e.msg);
                        setIsError(true)
                    }
                    if(e.param === 'password' ){
                        setErrorMessage(e.msg);
                        setIsError(true);
                    }
                })
            }
        } else {
            setIsError(true);
            setErrorMessage('Les mots de passes ne correspondent pas');
        }
    }

  return (
    loading ? (
        <Loading/>
    ) : (
        <>
            <h1 className="site-title">
                Kabanist
            </h1>

            { validUrl ? (
                <>
                <form 
                    onSubmit={handleSubmit} 
                    autoComplete="off"
                    >
                    <div className="explain">
                        Veuillez renseigner votre nouveau mot de passe.
                    </div>
                    <div className={ isError ? "login-error active" : "login-error"}>
                        { errorMessage }
                    </div>

                    <div className={ isError ? 'input is-error' : "input"}>
                        <span><FaLock/></span> 
                        <input
                            type={ isShown ? 'text' : 'password'}
                            placeholder="Mot de passe"
                            name="password"
                            onChange={handleChange}
                            required
                        />
                        <span onClick={() => setIsShown(!isShown)} >{ isShown ? <BsFillEyeFill/> : <BsFillEyeSlashFill/> }</span>
                    </div>

                    <div className={ isError ? 'input is-error' : "input"}>
                        <span><BsShieldFillCheck/></span> 
                        <input
                            type={ isConfirm ? 'text' : 'password'}
                            placeholder="Confirmation du mot de passe"
                            name="confirmPassword"
                            onChange={handleChange}
                            required
                        />
                        <span onClick={() => setIsConfirm(!isConfirm)} >{ isConfirm ? <BsFillEyeFill/> : <BsFillEyeSlashFill/> }</span>
                    </div>
                    <button 
                        type="submit"
                        className="btn"
                    >
                        Envoyer
                    </button>

                    </form>
                </>
            ): (
            // IF NO VALID TOKEN / VALID URL
            <>
                <h3 className="title-error">
                    Erreur 404
                </h3>
                <span className="qcq" >
                    La page demandée n'existe pas.
                </span>

                <button
                className="backBtn"
                onClick={
                    () => navigate('/')
                }
                >
                    Retour
                </button>
            </>
            )}
        </>
    ))
}

export default ResetPassword