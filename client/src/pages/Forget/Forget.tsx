import { useState } from "react"
import { FaArrowLeft, FaRegEnvelope } from "react-icons/fa"
import { NavLink, useNavigate } from 'react-router-dom';
import API from "../../Api";
import { useToasts } from "../../hooks/Toasts/ToastContext";

interface IProps {
  email: string;
}

const initialValue = {
  email: ''
}
const Forget = () => {
    
    const [isError, setIsError] = useState<boolean>(false);
    const [formDatas, setFormDatas] = useState<IProps>(initialValue);

    const { pushToast } = useToasts();
    const navigate = useNavigate();

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();
      try {
          await API.post(`${process.env.REACT_APP_API}/auth/reset`, formDatas);
          pushToast({
            title: "Mot de passe",
            content: "Vous avez formulé une demande de ré-initialisation de mot de passe, un email vous a été envoyé"
        })
        navigate('/');
      } catch (err: any) {
        const errors = err.data.errors;
          errors.forEach((e: any) => {
              if(e.param === 'email' ){
                  setIsError(true)
              }
          })
      }
    }

    const handleChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
        e.preventDefault();
        setFormDatas({
            ...formDatas,
            [e.target.name]: e.target.value
        })
        if(isError ) {
            setIsError(false);
        }
    }

  return (
    <>
      <h3 className="title">Mot de passe oublié ?</h3>
        <form 
            onSubmit={handleSubmit} 
            autoComplete="off"
        >
          <div className="explain">
            Veuillez renseigner l'adresse email avec laquelle vous êtes inscrit(e).
          </div>
            <div className={ isError ? "login-error active" : "login-error"}>
                Cet email n'est pas enregistré dans notre base de donnée utilisateur.
            </div>
              <div className={ isError ? 'input is-error' : "input"}>
                  <span><FaRegEnvelope/></span> 
                  <input
                      type='email'
                      placeholder="Email"
                      name="email"
                      onChange={handleChange}
                      required
                  />
              </div>
              <button 
                  type="submit"
                  className="btn"
              >
                  Envoyer
              </button>
            <NavLink to="/login" className="forgot"><FaArrowLeft/> &nbsp; Retour à la page de connexion</NavLink>
            <div className="create">
              Pas encore de compte? &nbsp;
                <NavLink to="/register">
                      S'enregistrer.
                </NavLink>
            </div>
        </form>
    </>
  )
}

export default Forget