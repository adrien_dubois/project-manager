let confirmRegister = (email, firstname, subject, url) => {
    const emailTemplate = {
        from: "contact@kabanist.fr",
        to: email,
        subject: subject,
        html:
            `${firstname}, Bienvenue sur Kabanist, <br>`+
            `Merci de votre inscription sur nos services, <br>`+
            `Nous sommes heureux de pouvoir vous compter parmis nos membres. <br>`+
            `<a href='${url}'> Kabanist </a> `
    };
    return emailTemplate;
}

let resetPassword = (email, subject, url) => {
    const emailTemplate = {
        from: "contact@kabanist.fr",
        to: email,
        subject: subject,
        html:
            `Kabanist, <br>`+
            `Vous avez demandé la réinitialisation de votre mot de passe. <br>`+
            `Afin de poursuivre cette demande, veuillez cliquer sur le lien ci-dessous. <br>`+
            `<a href='${url}'> Lien de mot de passe </a> `
    };
    return emailTemplate;
}

module.exports = { confirmRegister, resetPassword }